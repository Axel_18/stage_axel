# Valérie
**Créatrice de contenu**


![](https://ludi.paxpar.tech/card/card_1514_recto.png)
[Mail](valerie@paxpar.tech)
[Twitter](https://twitter.com/ValerieSuignard)
[Linkedin](https://www.linkedin.com/in/valerie-suignard/)


J’ai enseigné l’anglais en lycée pendant 15 ans. Quel était mon projet ? Permettre à ces jeunes lycéens d’avoir une plus grande ouverture d’esprit et un sens critique, d’être avide de découvertes et de dépassement de soi. Ma propre envie de découvrir autre chose m’a poussée, après deux années passerelles, à m’investir aujourd’hui dans la plateforme intelligente « paxpar ». C’est mon nouveau projet : un de ceux qui challenge à la fois mes qualités de communicante et me permet d’acquérir de nombreuses compétences techniques !

- Je l’ai constaté dans les établissements scolaires du second degré : les supports dématérialisés prennent doucement mais sûrement le pas sur les cahiers et les fiches cartonnées.
- Les entreprises ne sont pas exempts de ce phénomène, elles sont de plus en plus connectées : il devient essentiel de pouvoir garantir l’authenticité des documents numériques que chaque entreprise archive ou transmet à ses clients et fournisseurs.

A mon Sujet| Compétences clés
---|---
   |

## Blog

## Scénario
