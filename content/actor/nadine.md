# Nadine

Qui suis-je ? Au fil des années, en tant que gestionnaire pour une entreprise de conseil et surtout en tant que responsable du support pour une plateforme destinée à des courtiers en assurance et réassurance, j’ai aiguisé mon sens de l’organisation et mes qualités d’écoute. Lorsqu’on m’a proposé de travailler à l’élaboration de la plateforme intelligente « paxpar », j’ai accepté volontiers de m’engager dans cette nouvelle aventure.

acteur Nadine

![](https://ludi.paxpar.tech/card/card_1516_recto.png)