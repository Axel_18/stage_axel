# Francoise

Après 23 ans en Recherche et Développement, dans un groupe agro-alimentaire, en charge de projets R&D sur des systèmes industriels innovants et de la direction d’un service Qualité-Sécurité-Environnement, j’ai eu l’opportunité d’accroître mes connaissances, dans de nouveaux domaines, cette fois-ci tournés vers le service Client. Mon attention se porte maintenant depuis plus de 2 ans, sur la création d’une plateforme intelligente « paxpar »

acteur Francoise

![](https://ludi.paxpar.tech/card/card_1500_recto.png)