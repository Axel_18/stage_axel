---
layout: scenario
---

# Bon de commande

Architecture ...



## acteurs 

* [Nadège](/actor/nadege)
* [Paul](/actor/paul)
* Axel

## Etapes

1. gfksdjgkldfm sklm jgksdflm gjklmd
1. Nadège vérifie le document FA-676.pdf
    avec la checklist `custom.tholga.invoice`
1. gfksdjgkldfm sklm jgksdflm gjklmd

## Documents

* facture FA-676.pdf initiale
* facture FA-676b.pdf piratée

## Signatures

### FA-676.pdf

Les signatures du document FA-676.pdf :

1. signature de A ..
1. signature de X ..
1. signature de P ..


### FA-676b.pdf

Les signatures du document FA-676.pdf :

1. signature de A ..
1. signature de P2 .. (P2 identité usurpée de P)


```mermaid
graph TD;
  A-->B;
  A-->CCC;
  B-->D;
  C-->D;
  CCC-->D;
```