
## Chapitre 1

*Texte en italique*
_Texte en italique_
**Texte en gras**
__Texte en gras__
***Texte en italique et en gras***
___Texte en italique et en gras___

~~Ce texte est barré.~~ mais pas celui-là.

#  Titre 1
## Titre 2
###  Titre 3
#### Titre 4
#####  Titre 5
###### Titre 6

>Ceci est une **zone en retrait**.
>La zone continue ici

>Ceci est une autre **zone de retrait**.

Cette zone continue également dans la ligne suivante.
Cependant, cette ligne n’est plus en retrait

- Liste1
- Liste 2
- Liste 3

1. Liste 1
1. Liste 2
1. Liste 3

[ ] A

[x] B

[ ] C

C’est le `code`.

``C’est tout le `code`.``

![Ceci est un exemple d’image](https://example.com/bild.jpg)

fjdksl `jdklfgjkldmfjgklmdfj` gklm *dfjgklmdjfgklmsfjgkldfj* gklmdjgklmdjklmg jdklgm **jdklmgjdklmg** jksdlm gjsdklmjg klmdfsgjklmdsfjgkl gklmj dgklmdjs klmgjdklm

![ma voiture](https://m.media-amazon.com/images/I/71yTDvJ0hRL.__AC_SY300_SX300_QL70_ML2_.jpg)


xxx | xxxx | xxxx
----|-----------|-----
xxx | xxxfdsfsdf gkfdjgkldf jgldsdfx | xxxx
xxx | xxxx | xxxx
xxx | xxxx | xxxx


```python
def toto():
    x = 34
    print(x)
```


gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd 


## Chapitre 2

::callout
#summary
This is a callout! Click me to open.
#content
This is the content of the callout.
::

gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd 


## Chapitre 3


:badge[v1.2]

gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj gklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmds gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm 


## Chapitre 4

gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd  fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm 


## Chapitre 5

gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm 
gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm gdfjkgd jfklmg jdkmgjklmdsf jgklmdsf jgklmsdf jgklmsd jgklmsd fjgklmdsfjklmjklmdf jdfmjg dms kjgdmklf jgklmdfsj gklmdsjgklmdsj gklmjkglmjdsklmgj klmdsjgkldsfjgklmsdjklm 
