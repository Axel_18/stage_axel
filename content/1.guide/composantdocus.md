## 1er composant
::alert{type="info"}
Check out an **info** alert with `code` and a [link](/).
::

::alert{type="success"}
Check out a **success** alert with `code` and a [link](/).
::

::alert{type="warning"}
Check out a **warning** alert with `code` and a [link](/).
::

::alert{type="danger"}
Check out a **danger** alert with `code` and a [link](/).
::

## 2ème composant


:badge[v1.2]

:badge[Deprecated]{type="warning"}

::badge{type="danger"}
Not found!
::

## 3ème composant

::block-hero
---
cta:
  - Get started
  - /get-started
secondary:
  - Open on GitHub →
  - https://github.com/nuxtlabs/docus
snippet: npx nuxi@latest init docus-app -t nuxtlabs/docus-starter
---
#title
Document-driven framework

#description
Docus reconciles content creators and developers by offering to both the best tools to create and scale content-based websites.
::

## 4ème composant

:button-link[Play on StackBlitz]{icon="IconStackBlitz" href="https://stackblitz.com/github/nuxtlabs/docus-starter" blank}

## 5ème composant

::callout
#summary
This is a callout! Click me to open.

#content
This is the content of the callout.
::
 
::callout{type="warning"}
#summary
This is a callout! Click me to open.

#content
This is the content of the callout.
::

## 6ème composant

 ::card{icon="logos:nuxt-icon"}
 #title
 Nuxt Architecture.
 #description
 Based on **Nuxt 3** and **Nuxt Content**. :br
 Use Nuxt to build a static site, or a serverless app.
 ::

## 7ème composant

::card-grid
#title
What's included

#root
:ellipsis

#default
  ::card
  #title
  Nuxt Architecture.
  #description
  Harness the full power of Nuxt and the Nuxt ecosystem.
  ::
  ::card
  #title
  Vue Components.
  #description
  Use built-in components (or your own!) inside your content.
  ::
  ::card
  #title
  Write Markdown.
  #description
  Enjoy the ease and simplicity of Markdown and discover MDC syntax.
  ::
  ::card
  #title
  Nuxt Architecture.
  #description
  Harness the full power of Nuxt and the Nuxt ecosystem.
  ::
  ::card
  #title
  Vue Components.
  #description
  Use built-in components (or your own!) inside your content.
  ::
  ::card
  #title
  Write Markdown.
  #description
  Enjoy the ease and simplicity of Markdown and discover MDC syntax.
  ::
::

## 8ème composant

::code-group
  ```bash [Yarn]
  yarn add docus
  ```
  ```bash [NPM]
  npm install docus
  ```
::


## 9ème composant

/* Added as a child of `<CodeGroup />` */

::code-block{label="Preview" preview}
  ::badge
  Hello World!
  ::
::

## 10ème composant

:copy-button{content="hey!"}


## 11ème composant

:icon{name="logos:nuxt-icon"}
:icon{name="logos:vue"}
:icon{name="logos:nuxt-icon"}

## 12ème composant

::list{type="primary"}
- **Important**
- Always
::

::list{type="success"}
- Amazing
- Congrats
::

::list{type="info"}
- Do you know?
- You can also do this
::

::list{type="warning"}
- Be careful
- Use with precautions
::

::list{type="danger"}
- Drinking too much
- Driving drunk
::

## 13ème composant

:sandbox{src="https://codesandbox.io/embed/nuxt-content-l164h?hidenavigation=1&theme=dark"}

## 14ème composant
:terminal{content="nuxi build"}
