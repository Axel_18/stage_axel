# intro

sdkgjdfkl gjdklfmsjg kldm sfjgklsdfdgd fgdfjkgj dklfmgjdsf


## Points à voir


* [ ] jeu de carte avec Valérie
* [ ] scenario avec Nadine
* [ ] demo paxpar.tech vérif de PDF
* [x] vscode
* [ ] linux / ubuntu (inst)
* [ ] gitlab : créer un compte, créer un projet
* [x] git
* [x] markdown
* [ ] docus / content / nuxt / vuejs
* [ ] npm / yarn
* [ ] docus : https://docus.dev/
* [ ] content : https://content.nuxtjs.org/
* [ ] nuxt : https://nuxtjs.org/
* [ ] vuejs : https://vuejs.org/
* [ ] HTML/CSS  / vuejs
* [ ] animation SVG
* [ ] paxpar-widgets
* [ ] tailwindcss
* [ ] SSH
* [ ] installation PC sous Linux Ubuntu 


# strucutre docus

faire un site et des pages pour :

* une section notes perso de développement (mets tes notes, commentaires et surtout QUESTIONS)
* une section blog (structure année/mois, une page par article)
* des pages avec des PDF 
* un exemple d'utilisation de chaque composant docus
* une page avec des liens vers les ? 

# essaie md

Partie 1

fjdksl `jdklfgjkldmfjgklmdfj` gklm *dfjgklmdjfgklmsfjgkldfj* gklmdjgklmdjklmg jdklgm **jdklmgjdklmg** jksdlm gjsdklmjg klmdfsgjklmdsfjgkl gklmj dgklmdjs klmgjdklm

![ma voiture](https://m.media-amazon.com/images/I/71yTDvJ0hRL.__AC_SY300_SX300_QL70_ML2_.jpg)


xxx | xxxx | xxxx
----|-----------|-----
xxx | xxxfdsfsdf gkfdjgkldf jgldsdfx | xxxx
xxx | xxxx | xxxx
xxx | xxxx | xxxx


```python
def toto():
    x = 34
    print(x)
```


# Docus Starter

Starter template for [Docus](https://docus.dev).

## Clone

Clone the repository (using `nuxi`):

```bash
npx nuxi init -t themes/docus
```

## Setup

Install dependencies:

```bash
yarn install
```

## Development

```bash
yarn dev
```

## Edge Side Rendering

Can be deployed to Vercel Functions, Netlify Functions, AWS, and most Node-compatible environments.

Look at all the available presets [here](https://v3.nuxtjs.org/guide/deploy/presets).

```bash
yarn build
```

## Static Generation

Use the `generate` command to build your application.

The HTML files will be generated in the .output/public directory and ready to be deployed to any static compatible hosting.

```bash
yarn generate
```

## Preview build

You might want to preview the result of your build locally, to do so, run the following command:

```bash
yarn preview
```

---

For a detailed explanation of how things work, check out [Docus](https://docus.dev).
