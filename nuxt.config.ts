export default defineNuxtConfig({
  extends: '@nuxt-themes/docus',
  ssr: false // Disable Server Side rendering
  //target: 'static' // default is 'server'
})
